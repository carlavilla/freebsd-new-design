/*
 * Copyright (c) 1994-2023, The FreeBSD Documentation Project
 * Copyright (c) 2021-2023, Sergio Carlavilla <carlavilla@FreeBSD.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/* Main */
.container {
  display: grid;
  gap: 1rem;
  grid-template-columns: 1fr;
  grid-template-areas:
    "header"
    "main"
    "footer";
}

header {
  grid-area: header;
}

main {
  grid-area: main;
}

footer {
  grid-area: footer;
}

main {
  display: flex;
  flex-direction: column;
  width: 100%;

  .main-container {
    display: flex;
    width: 100%;
    max-width: var(--max-width);
    margin-right: auto;
    margin-left: auto;

    .content {
      display: flex;
      flex-direction: column;
      max-width: 70%;
      padding: 0 2rem;

      h1 {
        font-size: 2.5rem;
        margin-bottom: 2rem;
        border-bottom: 1px solid #e5e7eb;
      }

      h2 {
        margin: 1.2rem 0;
      }

      p {
        margin-top: 0;
        margin-bottom :1.2rem;
      }

      ul {
        margin: 0;
        padding-left: 2rem;
      }

      a {
        color: var(--links-color);

        &:hover {
          color: #660000;
        }
      }
    }

    aside {
      display: flex;
      flex-direction: column;
      margin: 2rem;
      padding-top: 7rem;

      #TableOfContents {
        border-left: 2px solid #e5e7eb;

        ul {
          margin: 0;
          padding: 1rem;
          list-style: none;

          li {
            padding-bottom: 1rem;

            a {
              text-decoration: none;

              &:hover {
                font-weight: 500;
              }
            }
          }
        }
      }
    }

    .not-found-container {
      display: flex;
      flex-direction: column;
      width: 100%;
      justify-content: center;
      align-items: center;

      h1 {
        padding: 0 1rem;
        font-size: 2.5rem;
        text-align: center;
      }

      h3 {
        margin-top: 2rem;
        margin-bottom: .2rem;
        padding: 0 2rem;
        font-size: 1.8rem;
      }

      p {
        padding: 0 2rem;
        font-size: 1.5rem;

        a {
          color: var(--links-color);

          &:hover {
            color: #660000;
          }
        }
      }

      img {
        padding: 0 2rem;
        width: 250px;
      }
    }
  }
}

/* Misc */
.fa {
  font-family: FontAwesome;
  font-style: normal;
}

hr {
  border: 0;
  border-top: 1px solid #e5e7eb;
}

a,
a:visited,
a:hover,
a:active {
  color: inherit;
}

/* Admonitions */
.admonitionblock {
  margin: 1.4rem 0;
  padding: 1rem;

  table {
    table-layout: fixed;
    position: relative;
    width: 100%;

    tbody {
      tr {
        td.icon {
          position: absolute;
          top: 0;
          left: 0;
          line-height: 1;
          padding-bottom: .5rem;

          i {
            display: inline-flex;
            align-items: center;
            width: auto;
            background-position-x: .5em;
            vertical-align: initial;
            font-style: normal;

            &:after {
              content: attr(title);
              font-family: 'Inter var', sans-serif;
              font-weight: bolder;
              padding: 0 .5em;
              margin: -.05em;
            }
          }

          .icon-note::before {
            content: "\f05a";
            color: var(--admonition-note-color);
          }

          .icon-tip::before {
            content: "\f0eb";
            color: var(--admonition-tip-color)
          }

          .icon-warning::before {
            content: "\f071";
            color: var(--admonition-warning-color);
          }

          .icon-caution::before {
            content: "\f06d";
            color: var(--admonition-caution-color);
          }

          .icon-important::before {
            content: "\f06a";
            color: var(--admonition-important-color);
          }
        }

        td.icon [class^="fa icon-"] {
          font-size: 1.2rem;
          cursor: default;
        }

        td.content {
          width: 100%;
          max-width: 100%;
          padding: inherit;
          word-wrap: anywhere;

          .title {
            margin-top: 2rem;
          }

          .paragraph {
            padding-top: 2rem;
          }

          a {
            color: var(--admonition-links-color);
          }
        }
      }
    }
  }
}

.note {
  border-left: 5px solid var(--admonition-note-color);
  background-color: var(--admonition-note-background-color);
}

.warning {
  border-left: 5px solid var(--admonition-warning-color);
  background-color: var(--admonition-warning-background-color);
}

.important {
  border-left: 5px solid var(--admonition-important-color);
  background-color: var(--admonition-important-background-color);
}

.caution {
  border-left: 5px solid var(--admonition-caution-color);
  background-color: var(--admonition-caution-background-color);
}

.tip {
  border-left: 5px solid var(--admonition-tip-color);
  background-color: var(--admonition-tip-background-color);
}

@media screen and (min-width: 450px) {
  .container {
    gap: 2rem;
    grid-template-columns: 1fr 1.5fr;
    grid-template-areas:
      "header header"
      "main main"
      "footer footer";
  }
}

@media screen and (min-width: 900px) {
  .container {
    gap: 1rem;
    grid-template-columns: 1fr 2fr 2fr;
    grid-template-areas:
      "header header header"
      "main main main"
      "footer footer footer";
  }
}

@media screen and (max-width: 1100px) {
  .content {
    max-width: 100% !important;
  }

  aside {
    display: none !important;
  }
}
