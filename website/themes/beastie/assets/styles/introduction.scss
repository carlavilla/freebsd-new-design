/*
 * Copyright (c) 1994-2023, The FreeBSD Documentation Project
 * Copyright (c) 2021-2023, Sergio Carlavilla <carlavilla@FreeBSD.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
.introduction-container {
  display: flex;
  max-width: var(--max-width);
  margin-right: auto;
  margin-left: auto;

  .introduction {
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
    text-align: center;
    align-content: center;

    .main-logo {
      display: block;
      width: 100%;

      img {
        width: 200px;
      }
    }

    h1 {
      margin: 0;
      font-size: 50px;
    }

    p {
      margin: 0;
      font-size: 30px;
    }
  }
}

.download-container {
  display: flex;
  flex-direction: column;
  margin-top: 2rem;
  width: 100%;
  max-width: var(--max-width);
  margin-right: auto;
  margin-left: auto;
  justify-content: center;
  text-align: center;

  .actions {
    display: flex;
    margin-bottom: 1rem;
    justify-content: center;

    .download {
      display: flex;
      align-items: center;
      padding: .5rem 1rem;
      background-color: #FACC2E;
      border-top-left-radius: .4rem;
      border-bottom-left-radius: .4rem;
      color: var(--black);
      text-decoration: none;
      text-align: center;
    }

    .download-options {
      display: inline-block;
      position: relative;
      padding: .5rem 1rem;
      margin-left: 1px;
      background-color: #FACC2E;
      border-top-right-radius: .4rem;
      border-bottom-right-radius: .4rem;
      color: var(--black);
      cursor: pointer;

      .download-options-input {
        display: none;

        &:checked + .download-options-list {
          display: block;
        }
      }

      .download-options-input + .download-options-list {
        display: none;
      }

      .download-options-list {
        position: absolute;
        min-width: 250px;
        top: 100%;
        left: -13rem;
        margin: .2rem 0;
        padding: 0;
        border: 1px solid #CCC;
        border-radius: 4px;
        box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
        background-color: #FFF;

        li {
          display: flex;
          gap: .7rem;
          padding: .6rem 1.3rem;

          span {
            margin-right: .8rem;
          }

          a {
            display: block;
            cursor: pointer;
            text-decoration: none;
          }
        }
      }

      li.split{
        display: block;
        gap: 0;
        margin: 0;
        padding: 0;
        border-bottom: 1px solid #cccccc;
      }

      li.other {
        padding-left: 1.3rem;
      }

    }

    .documentation {
      display: flex;
      align-items: center;
      margin-left: .5rem;
      padding: .5rem 1rem;
      color: var(--black);
      background-color: #FFF;
      border: 1px solid #444;
      border-radius: .4rem;
      text-decoration: none;

      i {
        margin-left: .2rem;
      }

      &:hover {
        background-color: var(--card-background-color);
      }
    }
  }

  .extra-info {
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 1rem;
    margin: 1rem 0;

    div {
      display: flex;
      justify-content: center;
      gap: 2rem;
    }
  }
}

.core-technologies-container {
  display: block;
  background-color: var(--card-background-color);
  border-top: 1px solid #f2f2f3;

  .core-technologies {
    display: flex;
    flex-direction: column;
    max-width: var(--max-width);
    margin-right: auto;
    margin-left: auto;
    text-align: center;

    h2 {
      font-size: 35px;
    }

    .technologies-container {
      display: grid;
      gap: 1rem;
      grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

      .technology {
        display: block;
        text-align: left;

        h3 {
          display: flex;
          align-items: center;
          padding: 2rem .5rem 0 .5rem;
          margin: 0;

          i,
          img {
            margin-right: .5rem;
            padding: .5rem;
          }
        }

        p {
          padding: 2rem .5rem;
          margin: 0;
        }
      }
    }
  }
}

.trust-freebsd-container {
  display: block;

  .trust-freebsd {
    display: flex;
    flex-direction: column;
    max-width: var(--max-width);
    margin-right: auto;
    margin-left: auto;
    text-align: center;

    h2 {
      font-size: 35px;
    }

    .companies-projects {
      display: grid;
      gap: 1rem;
      grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

      .company-project {
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 1.5rem 0;

        img {
          width: 200px;
        }
      }
    }
  }
}

.news-events-container {
  display: block;
  background-color: var(--card-background-color);
  border-top: 1px solid #f2f2f3;

  .news {
    display: flex;
    flex-direction: column;
    max-width: var(--max-width);
    margin-right: auto;
    margin-left: auto;
    text-align: center;

    h2 {
      font-size: 35px;
      margin-bottom: 0px;
    }

    .news-container {
      display: grid;
      gap: 1rem;
      grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

      .news-item {
        display: block;
        margin: 1rem;
        padding: .2rem 1rem;
        background-color: var(--white);
        border: 1.5px solid #c6c8ca;
        border-left: 5px solid #3380cc;
        border-radius: 0.25rem;
        text-align: left;
      }
    }

    .all-news-container {
      display: flex;

      .all-news-link {
        padding-left: 1rem;
        padding-right: .5rem;
      }
    }

    .events-container {
      display: grid;
      gap: 1rem;
      grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

      .event-item {
        display: block;
        margin: 1rem;
        padding: .2rem 1rem;
        background-color: var(--white);
        border: 1.5px solid #c6c8ca;
        border-left: 5px solid #43b929;
        border-radius: 0.25rem;
        text-align: left;
      }
    }

    .all-events-container {
      display: flex;
      margin-bottom: 1rem;

      .all-events-link {
        padding-left: 1rem;
        padding-right: .5rem;
      }
    }
  }
}

.cloud-container {
  display: flex;
  flex-direction: column;

  .clouds {
    display: flex;
    flex-direction: row;
    justify-content: center;
    max-width: var(--max-width);
    margin: 1rem auto;
    text-align: center;

    .cloud-description {
      display: block;
      padding: 2rem;
      width: 35%;
      border-right: 1px solid #95959d;
      text-align: left;

      h2 {
        font-size: 35px;
      }
    }

    .platforms {
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding-left: 4rem;
      width: 30%;
      text-align: left;

      a {
        display: block;
        padding: 1rem 0;

        .cloud-logo {
          height: 30px;
        }
      }
    }
  }
}

.freebsd-foundation-container {
  display: block;
  background-color: var(--card-background-color);
  border-top: 1px solid #f2f2f3;

  .freebsd-foundation {
    display: flex;
    max-width: var(--max-width);
    margin-right: auto;
    margin-left: auto;
    text-align: center;

    .freebsd-foundation-description {
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding: 2rem;
      width: 40%;
      text-align: left;

      h2 {
        font-size: 35px;
      }

      .all-events-link {
        display: flex;
        align-items: center;
        padding: .5rem 1rem;
        color: var(--black);
        background-color: #fff;
        border: 1px solid #444;
        border-radius: .4rem;
        text-decoration: none;

        i {
          margin-left: .2rem;
        }
      }
    }

    .news-entries {
      display: flex;
      flex-direction: column;
      padding: 2rem;
      width: 35%;
      text-align: left;

      h4 {
        margin-bottom: .5rem;
      }

      .all-news-link {
        padding-left: 1rem;
        text-align: left;
        text-decoration: none;
      }
    }

    .news-item {
      display: block;
      margin-bottom: 1rem;
      padding: .2rem 1rem;
      background-color: var(--white);
      text-align: left;
      border: 1.5px solid #c6c8ca;
      border-left: 5px solid #660000;
      border-radius: 0.25rem;

      h4 {
        margin-top: 1rem;
        margin-bottom: 0;
      }

      .date {
        margin-top: 0;
      }
    }
  }
}

.security-all-container {
  display: block;

  .security {
    display: flex;
    flex-direction: column;
    max-width: var(--max-width);
    margin-right: auto;
    margin-left: auto;
    text-align: center;

    h2 {
      font-size: 35px;
      margin-bottom: 0px;
    }

    .advisories-container {
      display: grid;
      gap: 1rem;
      grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

      .advisories-item {
        display: block;
        margin: 1rem;
        padding: .2rem 1rem;
        background-color: var(--white);
        border: 1.5px solid #c6c8ca;
        border-left: 5px solid #ffcc00;
        border-radius: 0.25rem;
        text-align: left;
      }
    }

    .all-advisories-container {
      display: flex;

      .all-advisories-link {
        padding-left: 1rem;
        padding-right: .5rem;
      }
    }

    .errata-container {
      display: grid;
      gap: 1rem;
      grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

      .errata-item {
        display: block;
        margin: 1rem;
        padding: .2rem 1rem;
        background-color: var(--white);
        border: 1.5px solid #c6c8ca;
        border-left: 5px solid #ff6600;
        border-radius: 0.25rem;
        text-align: left;
      }
    }

    .all-errata-container {
      display: flex;
      margin-bottom: 1rem;

      .all-errata-link {
        padding-left: 1rem;
        padding-right: .5rem;
      }
    }
  }
}

@media screen and (max-width: 767px) {
  .cloud-container {
    padding-bottom: 0;

    .clouds {
      flex-direction: column;

      .cloud-description {
        width: inherit;
        padding: 2rem;
        border: none;
      }

      .platforms {
        flex-direction: row;
        flex-wrap: wrap;
        gap: 2rem;
        width: inherit;
        padding: 0 2rem;
      }
    }
  }
}

@media screen and (max-width: 900px) {
  .core-technologies-container {
    padding-bottom: 0;

    .core-technologies {

      .technologies-container {
        gap: inherit;

        .technology {
          padding: 0 1rem;

          h3 {
            padding: 0;
          }
        }
      }
    }
  }

  .cloud-container {
    padding-bottom: 0;

    .clouds {
      .cloud-description {
        padding: 0 2rem;
      }
    }
  }

  .news-container {
    .news-item {
      margin: 0 1rem !important;
    }
  }

  .events-container {
    .event-item {
      margin: 0 1rem !important;
    }
  }

  .freebsd-foundation-container {
    .freebsd-foundation {
      flex-direction: column;

      .freebsd-foundation-description {
        width: inherit;
        padding: 0 2rem;
      }

      .news-entries {
        width: inherit;
        padding: 0 2rem;
      }
    }
  }
}
