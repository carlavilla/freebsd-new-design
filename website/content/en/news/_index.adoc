---
loadNews: false
---

= FreeBSD News
:doctype: article
:showtitle:
:toc: macro
:toclevels: 1
:icons: font
:experimental:

toc::[]

== FreeBSD Project and The FreeBSD Foundation

link:newsflash[FreeBSD News Flash]

https://freebsdfoundation.org/our-work/latest-updates/[Latest Updates | FreeBSD Foundation]

link:../status/[FreeBSD Status Reports]

== Other sites

https://www.bsdnow.tv/[BSD Now]

* Weekly podcast, episodes go out on Thursdays
* Recorded twice a month and streamed Wednesdays on a best effort basis
* News, interviews, tutorials, support and questions about any BSD operating system
* Helpful and informative for novices – Entertaining for pros.

https://dan.langille.org/[Dan Langille's Other Diary]

* successor to _The FreeBSD Diary_, which provided practical examples between 1988 and 2012
* https://www.langille.org/[Dan is], amongst other things, a committer to the FreeBSD ports tree.

https://www.osnews.com/topic/freebsd/[FreeBSD – OSnews]

https://slashdot.org/bsd/[BSD – Slashdot]

https://daemonforums.org/forumdisplay.php?f=40[News – DaemonForums]

* BSD and related.
